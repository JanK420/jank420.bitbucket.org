/* global L, distance */
/* global $*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var mapa;


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
; 

function generirajPodatke(stPacienta) {
  var number = Math.floor(Math.random() * 3) + 1;
  //console.log(number);
  if(number == 1){
    generirajPodatke1();
  }  
  if(number ==2){
    generirajPodatke2();
  }
  if(number == 3) {
    generirajPodatke3();
  }
}

function generirajPodatke1() {
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "Janez",
          lastNames: "Bogomoljka",
          dateOfBirth: "1973-05-13",
         // bolezen: "gripa",
          additionalInfo: {"ehrd": ehrId}
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            headers: {
              "Authorization": getAuthorization()
            },
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    console.log("succ");
                }
            }
        });
        
        $("#ime").val(partyData.firstNames);
        $("#priimek").val(partyData.lastNames);
        $("#datumRojstva").val(partyData.dateOfBirth);
        //$("#bolezen").val(partyData.bolezen);
        //$("#ehrid").val(ehrId);
      }
    });
}
function generirajPodatke2() {
  $.ajax({
      url: baseUrl + "/ehr",
      type: "POST",
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "Nik",
          lastNames: "Kovac",
          dateOfBirth: "1987-05-13",
          //bolezen: "meningitis",
          additionalInfo: {"EHRid": ehrId }, 
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            headers: {
              "Authorization": getAuthorization()
            },
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    console.log("succ");
                }
            }
        });
        //$("#ehrid").val(ehrId);
        $("#ime").val(partyData.firstNames);
        $("#priimek").val(partyData.lastNames);
        $("#datumRojstva").val(partyData.dateOfBirth);
        //$("#bolezen").val(partyData.bolezen);
        //$("#ehrid").val(ehrId);
      }
    });
}
function generirajPodatke3() {
  $.ajax({
      url: baseUrl + "/ehr",
      type: "POST",
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "DJ",
          lastNames: "Skakec",
          dateOfBirth: "1977-03-14",
          //bolezen: "",
          additionalInfo: {"EHRid": ehrId}, 
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            headers: {
              "Authorization": getAuthorization()
            },
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    console.log("succ");
                }
            }
        });
        $("#ime").val(partyData.firstNames);
        $("#priimek").val(partyData.lastNames);
        $("#datumRojstva").val(partyData.dateOfBirth);
        //$("#bolezen").val(partyData.bolezen);
        //$("#ehrid").val(ehrId);
      }
    });
}
function kreirajEHR() {
	var ime = $("#ime").val();
	var priimek = $("#priimek").val();
  var datumRojstva = $("#datumRojstva").val();
  
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#sporociloDodajPacienta").html("<span class='obvestilo label " +
      "label-warning fade-in'>Vnesite vse podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          partyAdditionalInfo: [
          {
            key: "ehrId",
            value: ehrId
          }
        ]
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#sporociloDodajPacienta").html("Uspešno kreiran EHRID: "+ ehrId);
              $("#sporociloNovPacient").html(partyData.firstNames+" "+partyData.lastNames+" " +"je bil uspešno kreiran");
              $("#ehrIdPac").val(ehrId);
            }
          },
          error: function(err) {
          	console.log(err);
          }
        });
      }
		});
	}
}
function grafPodatkov() {
  //$("#body-temperature").innerHTML="";
    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var ehrId = $("#ehrIdPac").val();
          $.ajax({
            url: baseUrl + "/view/" + ehrId + "/body_temperature",
            type: 'GET',
            headers: {
                "Authorization": getAuthorization()
            },
            success: function (res) {
                res.forEach(function(el, i, arr) {
                  var date = new Date(el.time);
                  el.date = date.getDate() + '-' + monthNames[date.getMonth()];
                });
                new Morris.Bar({
                  element: 'body-temperature',
                  data: res.reverse(),
                  xkey: 'date',
                  ykeys: ['temperature'],
                  labels: ['Body Temperature'],
                  hideHover: true,
                  barColors: ['#FFCE54'],
                  xLabelMargin: 5,
                  resize: true
                });
            }
          });
          var dropdown = $("#novPacient").val();
          if(dropdown == "gripa") {
            $('#slikaGripa').show("fast");
            $("#porociloBolniku").html("Če vaša gripa traja več dni kot na grafu povprečja, pojdite zdravniku.");
          }
          if(dropdown == "prehlad") {
            $('#slikaPrehlad').show("fast");
            $("#porociloBolniku").html("Če vaš prehlad traja več dni kot na grafu povprečja, pojdite zdravniku.");
          }
          if(dropdown == "niBolezni") {
            $('#slikaZdrav').show("fast");
            $("#porociloBolniku").html("Če se telesene temperature ujemajo z grafom (+/-0,5°C), ste ZDRAVI!")
          }
  
}
function dodajMeritve() {
  var ehrId = $("#ehrIdPac").val();
  var uraMeritve = $("#uraMeritve").val();
  var telesnaTemperatura = $("#tempPac").val();
  
  if(!ehrId || ehrId.trim().length==0) {
    console.log("nafilaj vse");
  } else {
      var podatki = {
        "ctx/language": "en",
  		  "ctx/territory": "SI",
  		  "ctx/time": uraMeritve,
  		  "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
  		  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
      };
      var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#sporociloNovPacient").html("Meritev je bila uspešno dodana");
      },
      error: function(err) {
      	console.log(err);
      }
		});
  }
  uraMeritve = $("#uraMeritve").val("");
  telesnaTemperatura = $("#tempPac").val("");
}

function prikazSlike() {
  $('#klikSlika').on("click", function(){
         $('#slikaGripa').show("fast");
      });
}

$.getJSON("knjiznice/json/bolnisnice.json", function(data){
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    
  });
  
    var geojson = L.geoJson(data, {
      onEachFeature: function (feature, layer) {
        var koordinate = feature.properties.coordinates;
        var text = feature.properties.name;
        var helou = feature.properties["addr:city"];
        var ulica = feature.properties["addr:street"]
        if(typeof text === 'undefined' || typeof helou === 'undefined' || typeof ulica === 'undefined') {
          return;
        } else {
          layer.bindPopup(String("<dl><dt>Naslov</dt>"
                                  + "<dd>" + text + "</dd>"
                                  + "<dt>Mesto</dt>"
                                  + "<dd>" + helou + "</dd>"
                                  + "<dt>Ulica</dt>"
                                  + "<dd>" + ulica + "</dd>"));
        }
      }
    });
    var mapa = L.map('mapa_id').fitBounds(geojson.getBounds());
    layer.addTo(mapa);
    geojson.addTo(mapa);    
});

function jeBlizu(lat, lng) {
  var razdalja = 20;
  if(distance(lat, lng, LAT_TOCKA, LNG_TOCKA, "K") <= razdalja) razdalja=distance;
  
  return razdalja;
}
/*
* @param callback povratni klic z vsebino zahtevane JSON datoteke
*/

function salaDneva() {
  $.getJSON("https://api.chucknorris.io/jokes/random", function(data){
    console.log(data.value);
    $("#salaDneva").html(data.value);
  });
}

$(document).ready(function(){
  
});